FactoryGirl.define do
  factory :product_preorder, parent: :product do
    is_preorder  true
    po_estimation '2 weeks'
    po_status 'READY'
    po_closed_at { Date.tomorrow.to_s }
    down_payment_price 10
    price 50
  end
end