describe Spree::Order do
  describe '.contents' do
    let!(:order){ create :order }
    let!(:preorder_product) { create :product_preorder }
    let!(:base_product) { create :product }

    context 'add preorder item' do
      let(:preorder_product_2) { create :product_preorder }
      it 'should be ok' do
        expect{ order.contents.add(preorder_product.master, 1) }
          .to_not raise_error(ActiveRecord::RecordInvalid)
      end

      it 'should set the preorder flag in oder record' do
        expect{ order.contents.add(preorder_product.master, 1) }
          .to change{ order.preorder }.from(false).to(true)
      end

      it 'should be ok when add 2 preorder item' do
        expect do
          order.contents.add(preorder_product.master, 1)
          order.contents.add(preorder_product_2.master, 1)
        end.to_not raise_error(ActiveRecord::RecordInvalid)
        expect(order.preorder).to eq(true)
      end

      it 'should be not ok when add non preorder after preorder item' do
        order.contents.add(preorder_product.master, 1)
        expect { order.contents.add(base_product.master, 1)}
          .to raise_error(ActiveRecord::RecordInvalid)
      end

      context 'when all item removed' do
        before do
          order.contents.add(preorder_product.master, 1)
          order.contents.add(preorder_product_2.master, 1)
        end

        it 'should set back preorder flag from true to false' do
          expect do
            order.contents.remove(preorder_product.master, 1)
            order.contents.remove(preorder_product_2.master, 1)
          end.to change{order.preorder}.from(true).to(false)
        end
      end
    end

    context 'add base item' do
      let(:base_product_2) { create :product }

      it 'should be ok' do
        expect{ order.contents.add(base_product.master, 1) }
          .to_not raise_error(ActiveRecord::RecordInvalid)
      end

      it 'should set not change the preorder flag from false' do
        expect(order.preorder).to eq(false)
        order.contents.add(base_product.master, 1)
        expect(order.preorder).to eq(false)
      end

      it 'should be ok when add 2 base item' do
        expect do
          order.contents.add(base_product.master, 1)
          order.contents.add(base_product_2.master, 1)
        end.to_not raise_error(ActiveRecord::RecordInvalid)
      end

      it 'should not be ok when add preorder after base/non preorder item' do
        order.contents.add(base_product.master, 1)
        expect{ order.contents.add preorder_product.master, 2 }
          .to raise_error(ActiveRecord::RecordInvalid)
      end
    end
  end
end
