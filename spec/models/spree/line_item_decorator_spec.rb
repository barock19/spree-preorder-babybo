RSpec.describe Spree::LineItem, type: :model do
  let!(:order) { create :order }
  let!(:preorder_product) { create :product_preorder }
  let!(:base_product) { create :product }
  describe 'create' do
    let(:line_item_with_preorder_product) { build(:line_item, order: order, variant: preorder_product.master ) }
    let(:line_item_with_non_preorder_product) { build(:line_item, order: order, variant: base_product.master) }

    context 'order already exist with non preorder product' do
      before { line_item_with_non_preorder_product.save }
      it 'should not valid due cross product type policy' do
        expect(line_item_with_preorder_product).to_not be_valid
        expect(line_item_with_preorder_product.errors[:base]).to include('cannot add preorder item to cart contain non preorder item')
      end
    end

    context 'order alredy exist with preorder product' do
      before { line_item_with_preorder_product.save }
      it 'should not valid due cross product type policy' do
        expect(line_item_with_non_preorder_product).to_not be_valid
        expect(line_item_with_non_preorder_product.errors[:base]).to include('cannot add non preorder item to cart contain preorder item')
      end
    end

    context 'when order already clear with the preorder' do
      before{ order.update(preorder: true) }
      it 'should be valid when add line with preorder' do
        expect(line_item_with_preorder_product).to be_valid
      end
      it 'should not be valid when add line with non preorder product' do
        expect(line_item_with_non_preorder_product).to_not be_valid
        expect(line_item_with_non_preorder_product.errors[:base]).to include('cannot add non preorder item to cart contain preorder item')
      end
    end
  end
end