describe Spree::PreorderPrice, type: :model do
  it {should belong_to(:price).class_name('Spree::Price')
    .with_foreign_key(:price_id)}
end