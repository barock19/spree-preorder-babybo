RSpec.describe Spree::Product, type: :model do
  describe 'preorder' do
    context 'update' do
      context 'from not preorder become preorder' do
        let(:product) { create :product, is_preorder: false }
        before do
          product.attributes = {
            is_preorder: true,
            po_estimation: '2 weeks',
            po_status: 'READY',
            po_closed_at: Date.tomorrow.to_s,
            down_payment_price: 10
          }
        end
        context 'not all preorder fields filled' do
          before { product.po_estimation = nil }
          it 'should not be valid' do
            expect(product).to_not be_valid
            expect(product.errors[:po_estimation]).to include('required when set to be preorder')
          end
        end
      end

      context 'from preorder become not preorder' do
        let!(:product) do
          create :product, is_preorder: true,
            po_estimation: '2 weeks',
            po_status: 'READY',
            po_closed_at: Date.tomorrow.to_s,
            down_payment_price: 10
        end
        before { product.is_preorder = false }
        it 'should remove preorder prices' do
          expect(product.preorder_prices).to be_present
          product.save; product.reload
          expect(product.preorder_prices).to_not be_present
        end
      end
    end
  end
end