describe Spree::Price, type: :model do
  it {should have_one(:preorder_price).class_name('Spree::PreorderPrice')
    .with_foreign_key(:price_id)}
end