var POJS = {
  initPreorderForm: function () {
    if($('[data-preorder]').size() == 0)
      return ;
    var POFields = ['#product_down_payment_price', '#product_po_estimation', '#product_po_status', '#product_po_closed_at']
    var showPOFields = function(){
      $('[data-preorder]').attr('data-preorder', true)
      $.each(POFields,  function(i, e){ $(e).removeAttr('disabled') })
    }
    var hidePOFields = function(){
      $('[data-preorder]').attr('data-preorder', false)
      $.each(POFields,  function(i, e){ $(e).attr('disabled', 'disabled') })
    }

    $('#product_is_preorder').on('change', function(event){
      var obj = $(event.target)
      obj.is(':checked') ? showPOFields() : hidePOFields()
    })
  }

}

$(document).ready(function(){
  POJS.initPreorderForm()
})