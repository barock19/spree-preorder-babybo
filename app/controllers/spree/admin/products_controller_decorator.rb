module Spree
  module Admin
    ProductsController.class_eval do
      create.before :clean_is_preorder_from_params
      update.before :clean_is_preorder_from_params
      private

      def clean_is_preorder_from_params
        if params[:product] && params[:product][:is_preorder] && params[:product][:is_preorder] == "0"
          params[:product][:is_preorder] = false
        end
      end
    end
  end
end
