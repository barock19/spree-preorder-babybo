Deface::Override.new(
  virtual_path: 'spree/admin/shared/_table_filter',
  name: 'add-scope-link-admin-order',
  insert_after: '#table-filter',
  original: '08e398f7c3deb027b8a3cc43e4ac4276c5fe49b4',
  partial: 'spree/admin/orders/order_scope_index'
)
