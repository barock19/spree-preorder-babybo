Deface::Override.new(
  virtual_path: 'spree/admin/prices/_table',
  name: 'show_preorder_price_on_admin_price_table_header',
  insert_before: '[data-hook="prices_header"] th.actions',
  text: '<% if @product.preorder? %><th>Down Payment Price</th><% end %>'
)

Deface::Override.new(
  virtual_path: 'spree/admin/prices/_table',
  name: 'show_preorder_price_on_admin_price_table_body',
  insert_before: 'tbody td.actions',
  text: '<% if @product.preorder? %><td class="align-right"><%= number_to_currency(price.down_payment_price) %></td><% end %>'
)

Deface::Override.new(
  virtual_path: 'spree/admin/prices/_form',
  name: 'show_preorder_price_on_admin_price_table_form',
  insert_after: '[data-hook="admin_product_price_form_amount"]',
  text: <<-ERB
    <div class="three columns" data-hook="admin_product_down_payment_price_form_amount">
      <%= f.field_container :down_payment_price do %>
        <%= f.label :down_payment_price %>
        <%= f.text_field :down_payment_price, value: f.object.down_payment_price_money.format, class: 'fullwidth title' %>
      <% end %>
    </div>
  ERB
)
