Deface::Override.new(
  virtual_path: 'spree/admin/products/new',
  name: 'add_preorder_form_on_admin_new_product',
  original: '18891a8751dee517d952d4964ab2ba07cb24b3c4',
  insert_after: '[data-hook="new_product_attrs"]',
  text: <<-ERB
  <div class="clearfix"></div>
    <div data-hook="new_product_preorder_info_form">
      <%= f.check_box :is_preorder %>
      <%= f.label :is_preorder, 'PREORDER' %>
    </div>
  <div class="clearfix"></div>
  ERB
)