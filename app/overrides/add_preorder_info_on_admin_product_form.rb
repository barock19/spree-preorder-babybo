Deface::Override.new(
  virtual_path: 'spree/admin/products/_form',
  name: 'add_preorder_info_on_admin_product_form',
  original: '7c6353dc138c5b9f86d50f3f70976549331df2d7',
  insert_before: '[data-hook="admin_product_form_promotionable"]',
  partial: 'spree/admin/products/preorder_info_on_admin_product_form'
)
Deface::Override.new(
  virtual_path: 'spree/admin/products/_form',
  name: 'add_preorder_dp_price_on_admin_product_form',
  original: '44d9f8df4ec221e3f010c7354524718a176105be',
  insert_before: '[data-hook="admin_product_form_price"]',
  partial: 'spree/admin/products/preorder_dp_price_on_admin_product_form'
)

Deface::Override.new(
  virtual_path: 'spree/admin/products/_form',
  name: 'add_attribute_inform_preorder_product',
  original: 'e09e5b947b83258d77de9e5e8e81f06effe29c49',
  set_attributes: '[data-hook="admin_product_form_fields"]',
  attributes: {'data-preorder' => '<%= @product.preorder? ? true : false %>'}
)


