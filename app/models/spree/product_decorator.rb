Spree::Product.class_eval do
  PO_STATUS = %w(READY PREORDER CANCELED)
  has_many :preorder_prices, through: :variants_including_master
  delegate :down_payment_price, :down_payment_price=, :full_price_for, :leftover_price, :leftover_price_for, to: :find_or_build_master
  validates_presence_of :po_estimation, :po_status, :po_closed_at, :down_payment_price,
    if: :is_preorder, on: :update, message: 'required when set to be preorder'
  after_update :remove_preorder_prices, if: :unset_preorder

  def preorder?
    is_preorder?
  end

  private
  def remove_preorder_prices
    preorder_prices.each(&:destroy)
  end

  def unset_preorder
    !new_record? && is_preorder_was == true && is_preorder == false
  end
end
