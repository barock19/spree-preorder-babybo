Spree::Price.class_eval do
  has_one :preorder_price,
    class_name: "Spree::PreorderPrice",
    foreign_key: :price_id,
    dependent: :destroy,
    inverse_of: :price,
    autosave: true

  PREORDER_MONEY_ATTR = [
    :down_payment_price, :leftover_price
  ]

  PREORDER_MONEY_ATTR.each do |attr|
    delegate attr, :"#{attr}_money", to: :find_or_build_preorder_price
  end

  delegate :down_payment_price=, to: :find_or_build_preorder_price

  private
  def find_or_build_preorder_price
    preorder_price || build_preorder_price
  end
end