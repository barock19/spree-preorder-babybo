Spree::LineItem.class_eval do
  validate :check_cross_product_type_policy, on: :create
  delegate :preorder?, to: :product

  private
  def check_cross_product_type_policy
    if (preorder? \
      && !order.line_items.empty? \
      && !order.line_items.first.preorder? )

      return errors.add(:base, 'cannot add preorder item to cart contain non preorder item')
    end

    if (!preorder? && order.preorder?) || \
      (!preorder? \
        && !order.line_items.empty? \
        && order.line_items.first.preorder? )
      return errors.add(:base, 'cannot add non preorder item to cart contain preorder item')
    end
  end
end