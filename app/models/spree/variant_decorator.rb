Spree::Variant.class_eval do
  delegate :down_payment_price=, :down_payment_price, :leftover_price, to: :find_or_build_default_price
  delegate :preorder?, to: :product
  delegate :full_price_for, :leftover_price_for, to: :price_selector
  has_many :preorder_prices, through: :prices
end