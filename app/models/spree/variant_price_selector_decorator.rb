Spree::Variant::PriceSelector.class_eval do

  def price_for_with_preorder(price_options)
    return price_for_without_preorder(price_options) unless variant.preorder?
    price_matcher(price_options).try!(:down_payment_price_money)
  end

  alias_method_chain :price_for, :preorder
  alias_method :full_price_for, :price_for_without_preorder

  def leftover_price_for(price_options)
    return price_for_without_preorder(price_options) unless variant.preorder?
    price_matcher(price_options).try!(:leftover_price_money)
  end

  private
  def price_matcher(price_options)
    variant.currently_valid_prices.detect do |price|
      ( price.country_iso == price_options.desired_attributes[:country_iso] ||
        price.country_iso.nil?
      ) && price.currency == price_options.desired_attributes[:currency]
    end
  end
end