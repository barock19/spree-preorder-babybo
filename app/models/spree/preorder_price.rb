class Spree::PreorderPrice < ActiveRecord::Base
  validates :amount, presence: true
  belongs_to :price, class_name: "Spree::Price", foreign_key: :price_id
  delegate :currency, to: :price

  extend Spree::DisplayMoney
  money_methods :amount, :down_payment_price, :leftover_price

  alias_method :down_payment_price_money, :display_down_payment_price
  alias_method :leftover_price_money, :display_leftover_price

  def down_payment_price=(n)
    self[:amount] = Spree::LocalizedNumber.parse(n)
  end

  def down_payment_price
    amount
  end

  def leftover_price
    price.amount - amount
  end
end
