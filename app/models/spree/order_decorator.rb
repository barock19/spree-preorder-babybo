Spree::Order.class_eval do
  self.whitelisted_ransackable_attributes += %w(preorder)
  register_update_hook :update_preorder_flag_if_needed
  after_initialize :set_preorder_flag, if: :new_record?

  private
  def set_preorder_flag
    self.preorder = false
  end

  def update_preorder_flag_if_needed
    if line_items.empty?
      self.preorder = false
    elsif line_items.count == 1 && line_items.first.preorder?
      self.preorder = true unless preorder?
    end
  end
end