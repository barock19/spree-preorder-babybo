class AddPreorderToSpreeOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_orders, :preorder, :boolean
  end
end
