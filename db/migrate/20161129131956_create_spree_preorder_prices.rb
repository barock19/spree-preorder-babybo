class CreateSpreePreorderPrices < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_preorder_prices do |t|
      t.integer :price_id
      t.decimal :amount
      t.timestamps
    end
  end
end
